@isTest
private class SlackMessage_Test {
    @isTest // coverage only, all the action happens in jira, so not much to assert
    private static void testSlackMessage() {

        Test.startTest();
        SlackMessage.SlackMessageRequest slackRequest = new SlackMessage.SlackMessageRequest();
        slackRequest.channel = '@rustan.valino';
        slackRequest.message = 'test message';
        SlackMessage.send(new List<SlackMessage.SlackMessageRequest>{slackRequest});
        Test.stopTest();
    }

    @IsTest
    private static void testFutureCall() {
        // test the future bypass
        List<SlackMessage.SlackMessageRequest> requests = new List<SlackMessage.SlackMessageRequest>();
        for (Integer i = 0; i < 10; i++) {
            SlackMessage.SlackMessageRequest slackRequestFuture = new SlackMessage.SlackMessageRequest();
            slackRequestFuture.channel = '@ben.faber';
            slackRequestFuture.message = 'slackRequestFuture'+i;
            requests.add(slackRequestFuture);
        }

        Test.startTest();
        SlackMessage.sendFuture(JSON.serialize(requests));
        Test.stopTest();
    }


}