global with sharing class SlackMessage implements Queueable, Database.AllowsCallouts {
    @Future
    global static void sendFuture(String serializedRequests) {
        // this will raise platform events instead of enqueuing right away.
        List<SlackMessageRequest> requests = (List<SlackMessageRequest>) JSON.deserialize(serializedRequests, List<SlackMessageRequest>.class);
        send(requests);
        return;
    }

    @InvocableMethod(label='Slack Message')
    global static List<SlackMessageResult> send(List<SlackMessageRequest> requests) {
        SlackMessageResult[] results = new SlackMessageResult[0];
        for (SlackMessageRequest request : requests) {
            results.add(new SlackMessageResult());
        }
        if (System.isQueueable() || System.isScheduled() || System.isBatch() || System.isFuture()) {
            List<SlackMessage__e> slackMessageEvents = new List<SlackMessage__e>();
            for (SlackMessageRequest request : requests) {
                slackMessageEvents.add(request.convertToEvent());
            }
            EventBus.publish(slackMessageEvents);
        } else {
            System.enqueueJob(new SlackMessage(requests));
        }
        return results;
    }

    private SlackMessageRequest[] requests;
    global SlackMessage(SlackMessageRequest[] requests) {
        this.requests = requests;
    }

    global void execute(QueueableContext context) {
        for (SlackMessageRequest request : requests) {
            sendMessage(cleanMessage(request.message), request.channel);
        }
    }

    private static void sendMessage(String msg, String channelId) {
        HttpRequest req = new HttpRequest();
        req.setEndpoint('callout:Slack/api/chat.postMessage');
        req.setHeader('Content-type', 'application/json; charset=utf-8');
        req.setHeader('Authorization', 'Bearer {!$Credential.Password}');
        req.setMethod('POST');
        Map<String, String> body = new Map<String, String>();
        body.put('text', msg);
        body.put('channel', channelId);
        req.setBody(JSON.serialize(body));
        Http http = new Http();

        if (Test.isRunningTest()) {
            return;
        }

        HttpResponse res = http.send(req);
        System.debug(res);
        System.debug(res.getBody());
    }

    private String cleanMessage(String message) {
        return message.replaceAll('_BR_ENCODED_', '\n');
    }

    global class SlackMessageResult {
        @InvocableVariable(required=true)
        global Boolean success = true;
    }

    global class SlackMessageRequest {
        @InvocableVariable(required=true)
        global String message;

        @InvocableVariable(required=true)
        global String channel;

        global SlackMessage__e convertToEvent() {
            final Integer messageMaxLength = SlackMessage__e.Message__c.getDescribe().getLength();
            final Integer messageLongMaxLength = SlackMessage__e.MessageLong__c.getDescribe().getLength();
            final Integer channelMaxLength = SlackMessage__e.Channel__c.getDescribe().getLength();
            SlackMessage__e sm = new SlackMessage__e();
            sm.Message__c = this.message.length() > messageMaxLength ? this.message.substring(0, messageMaxLength) : this.message;
            sm.Channel__c = this.channel.length() > channelMaxLength ? this.channel.substring(0, channelMaxLength) : this.channel;
            sm.MessageLong__c = this.message.length() > messageLongMaxLength ? this.message.substring(0, messageLongMaxLength) : this.message;
            return sm;
        }
    }
}