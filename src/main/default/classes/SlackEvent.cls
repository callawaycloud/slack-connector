/**
 * Created by ben.faber on 7/22/22.
 */

global with sharing class SlackEvent {
    @InvocableMethod(label='Publish Message Event')
    global static void publish(List<SlackMessageEvent> requests) {
        List<SlackMessage__e> slackMessageEvents = new List<SlackMessage__e>();
        for (SlackMessageEvent request : requests) {
            slackMessageEvents.add(request.convertToEvent());
        }
        EventBus.publish(slackMessageEvents);
    }

    global class SlackMessageEvent {
        @InvocableVariable(required=true)
        global String message;

        @InvocableVariable(required=true)
        global String channel;

        global SlackMessage__e convertToEvent() {
            final Integer messageMaxLength = SlackMessage__e.Message__c.getDescribe().getLength();
            final Integer messageLongMaxLength = SlackMessage__e.MessageLong__c.getDescribe().getLength();
            final Integer channelMaxLength = SlackMessage__e.Channel__c.getDescribe().getLength();
            SlackMessage__e sm = new SlackMessage__e();
            sm.Message__c = this.message.length() > messageMaxLength ? this.message.substring(0, messageMaxLength) : this.message;
            sm.Channel__c = this.channel.length() > channelMaxLength ? this.channel.substring(0, channelMaxLength) : this.channel;
            sm.MessageLong__c = this.message.length() > messageLongMaxLength ? this.message.substring(0, messageLongMaxLength) : this.message;
            return sm;
        }
    }

}