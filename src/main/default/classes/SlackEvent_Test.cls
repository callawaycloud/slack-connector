/**
 * Created by ben.faber on 7/22/22.
 */

@IsTest
private class SlackEvent_Test {
    @IsTest
    static void testBehavior() {
        Test.startTest();
        List<SlackEvent.SlackMessageEvent> requests = new List<SlackEvent.SlackMessageEvent>();
        for (Integer i = 0; i < 10; i++) {
            SlackEvent.SlackMessageEvent slackEvent = new SlackEvent.SlackMessageEvent();
            slackEvent.channel = '@ben.faber';
            slackEvent.message = 'SlackMessageEvent' + i;
            requests.add(slackEvent);
        }
        SlackEvent.publish(requests);
        SlackEvent.publish(requests);
        SlackEvent.publish(requests);
        SlackEvent.publish(requests);
        Test.stopTest();
    }
}