# Setup

- install package Package Installation URL: https://login.salesforce.com/packaging/installPackage.apexp?p0=04t1C000000goMAQAY
- enter "opensesame" for the password
- install the "Slack" app in workspace
  - go to https://api.slack.com/apps/AQD8MTWCR
  - install and note token for app
- edit the "Slack" named credential
- enter the slack token as the password

# Usage

- add a Apex action to a process builder or flow
- select "Slack Message" as apex class
- enter desired message
  - use formulas to get merge fields
  - use BR() to insert line breaks in the message
- enter recipient
  - can be #channel or @username

# Release
- update version in sfdx-project.json

### Release for Beta
- `sfdx force:package:version:create -p "Slack Connector" -k "opensesame" -w 10`
### With Coverage
- `sfdx force:package:version:create -p "Slack Connector" -k "opensesame" -w 10 --codecoverage `
### Promote for Release to Production
- `sfdx force:package:version:promote --package <packageversion>`

# Links
- [unlocked package workflow](https://developer.salesforce.com/docs/atlas.en-us.sfdx_dev.meta/sfdx_dev/sfdx_dev_unlocked_pkg_workflow.htm)

# Questions

- do i want this to have a namespace?
- should we have a general 'ccc' namespace?

# Todos

## Internal MVP

- unlocked package
- setup instructions

## Pilot MVP

- change "channel" to recipient
- copy right
- managed package
- distributable slack app (with better name)

## Client MVP

- error handling
  - no token
  - invalid token
  - bad recipient
- good setup user experience
- bugs from internal and pilot mvp addressed

## Formating

- support salesforce deep links
- allow for images

# Random Notes

- 1.0.0.5 uses platform events to shepard work.     Org limits are 100k per hour, 250k per 24-hour period.

## First Version Details

Successfully created the package version [08c1C000000PAuEQAW]. Subscriber Package Version Id: 04t1C000000ODlzQAG
Package Installation URL: https://login.salesforce.com/packaging/installPackage.apexp?p0=04t1C000000ODlzQAG
As an alternative, you can use the "sfdx force:package:install" command.

https://login.salesforce.com/packaging/installPackage.apexp?p0=04t5Y0000015lmOQAQ